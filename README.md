# whatTwin

This repository contains higher resolution images of the models presented in MIDas4CS workshop (part of EDOC 2024 conference).

## Reference
Itäpelto, T. et al. 2024. Reference Architecture of Cybersecurity Digital Twin. MIDas4CS. EDOC2024. Wien, Austria.

